pub mod anagram;

#[macro_use]
extern crate clap;

fn main() {
    let matches = clap_app!(anagram =>
        (about: "Prints anagrams for the provided word")
        (@arg WORD_LIST: -w --("word-list") +takes_value "Specifies a custom word list to use for finding anagrams")
        (@arg MIN_SIZE: -m --("min-size") +takes_value "Specifies the minimum size word to output (default 3)")
        (@arg INPUT: +required +multiple "Word from which to generate anagrams"))
    .get_matches();

    match anagram::Dictionary::from_word_list(
        matches.value_of("WORD_LIST").unwrap_or("data/en_US.txt"),
    ) {
        Ok(dict) => {
            let min_size = match matches.value_of("MIN_SIZE") {
                Some(m) => m.parse::<usize>().unwrap_or_else(|_| {
                    panic!("Provided minimum size was not a valid number: {}", m)
                }),
                None => 3,
            };
            for word in matches.values_of("INPUT").unwrap() {
                let anagrams = dict.find_anagrams_with_combinations(word, min_size, word.len());

                if !anagrams.is_empty() {
                    println!("Found the following anagrams for {}", word);
                    for anagram in &anagrams {
                        println!("\t{}", anagram);
                    }
                } else {
                    println!("Found no anagrams for {}", word);
                }
            }
        }
        Err(kind) => {
            println!("Unable to open words file: {}", kind);
        }
    }
}
