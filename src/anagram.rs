use itertools::Itertools;
use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

pub struct Dictionary {
    dict: HashMap<String, Vec<String>>,
}

impl Dictionary {
    const MINIMUM_WORD_SIZE: usize = 3;

    /// Creates a new anagram dictionary from a provided word list.
    pub fn from_word_list<P>(path: P) -> io::Result<Dictionary>
    where
        P: AsRef<Path>,
    {
        let file = File::open(path)?;
        return Ok(Self::new(
            io::BufReader::new(file).lines().map(|l| l.unwrap()),
        ));
    }

    /// Creates a dictionary from a string iterator.
    pub fn new<I>(words: I) -> Dictionary
    where
        I: IntoIterator,
        I::Item: Into<String>,
    {
        let mut dict = HashMap::new();

        for word in words {
            let word = word.into();

            // Filter out well spelled words that aren't fully alphabetic (ex. you're) or may be
            // proper nouns. As anagrams are only returned for words that are at least 3 letters
            // long, also filter out any words that are smaller than this length to minimize the
            // size of the dictionary.
            if (word.chars().all(|c| c.is_alphabetic() && c.is_lowercase()))
                && (word.len() >= Self::MINIMUM_WORD_SIZE)
            {
                // Create a key that is the words letters sorted alphabetically
                let word_sorted = itertools::sorted(word.chars()).collect::<String>();

                // Add the word
                dict.entry(word_sorted).or_insert(Vec::new()).push(word);
            }
        }

        Dictionary { dict }
    }

    /// Finds anagrams of the provided word.
    pub fn find_anagrams<S: Into<String>>(&self, word: S) -> Vec<&String> {
        match self.dict.get(
            &word
                .into()
                .to_lowercase()
                .chars()
                .sorted()
                .collect::<String>(),
        ) {
            Some(v) => v.iter().map(|s| s).collect(),
            None => Vec::new(),
        }
    }

    /// Finds anagrams for all combinations of the provided word from min to max inclusive.
    pub fn find_anagrams_with_combinations<S: Into<String>>(
        &self,
        word: S,
        min: usize,
        max: usize,
    ) -> Vec<&String> {
        let mut ret = Vec::new();
        let word = word.into().to_lowercase();

        for i in min..=max {
            word.chars()
                .combinations(i)
                .for_each(|x| ret.extend(self.find_anagrams(&x.iter().collect::<String>())));
        }

        ret.into_iter().unique().collect()
    }
}
